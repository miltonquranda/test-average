package com.wawa.test.utils;

import com.wawa.test.modal.Product;

import java.util.List;

public interface iProductCalculator {

    public double averageOfProduct(List<Product> products);

    public double movingAverage(List<Product> products);

}
