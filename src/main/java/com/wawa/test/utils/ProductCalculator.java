package com.wawa.test.utils;

import com.wawa.test.modal.Product;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class ProductCalculator implements iProductCalculator {


    @Override
    public double averageOfProduct(List<Product> products) {
        final AtomicReference<Double> sumOfProducts = new AtomicReference<>();
        sumOfProducts.set(0d);
        products.forEach(product ->
            sumOfProducts.set(Double.sum(sumOfProducts.get(), product.finalPrice())));

        sumOfProducts.set(sumOfProducts.get() / products.size());
        return sumOfProducts.get();
    }

    @Override
    public double movingAverage(List<Product> products) {
        //TBD
        //int period;
       // SimpleMovingAverage simpleMovingAverage = new SimpleMovingAverage(period)
        return 0;
    }
}
