package com.wawa.test.utils;

public enum PricingModal {

    STANDARD(1.0d), SILVER(0.9d), GOLD(0.8d),
    standard(1.0d), silver(0.9d), gold(0.8d);

    double value = 0f;

    PricingModal(double value) {
        this.value = value;
    }


    @Override
    public String toString() {
        return super.toString();
    }

    public double floatValue() {
        return value;
    }
}
