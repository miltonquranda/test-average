package com.wawa.test.modal;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = false)
@Data
@MappedSuperclass
class Category extends Pricing implements Serializable {

    public String categoryName = null;

}
