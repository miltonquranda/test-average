package com.wawa.test.modal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.wawa.test.utils.PricingModal;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = false)
@Data
@MappedSuperclass
class Pricing implements Serializable {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    PricingModal pm =  null;

}
