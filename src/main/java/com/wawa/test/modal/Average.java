package com.wawa.test.modal;

import lombok.Data;

import java.io.Serializable;

@Data
public class Average extends Category implements Serializable {

    double averageValue;

    public Average(double averageValue, String categoryName) {
        this.averageValue = averageValue;
        this.categoryName = categoryName;
    }


    public Average() {
    }
}
