package com.wawa.test.modal;

import com.wawa.test.utils.PricingModal;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = false)
@Data
@Entity
public class Product extends Category implements Serializable {

    @Id
    private Long id;
    private double price;
    private String attribute;

    public Product(Long id, String categoryName, PricingModal pm, double price, String attribute) {
        this.id = id;
        this.categoryName = categoryName;
        this.pm = pm;
        this.price = price;
        this.attribute = attribute;
    }

    public Product() {

    }

    public double finalPrice() {
        if(pm != null) {
            return price * pm.floatValue();
        }
        return price;
    }
}
