package com.wawa.test.service;

import com.wawa.test.modal.Product;
import com.wawa.test.modal.ProductRepository;
import com.wawa.test.utils.ProductCalculator;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class AbstractAppController {

    ProductRepository repository;

    @Autowired
    ProductCalculator productCalculator;

    public AbstractAppController(ProductRepository repository) {
        this.repository = repository;
    }


    Collection<String> getAllCategories(List<Product> products) {
        Set<String> categories = new HashSet<>();
        products.forEach(product ->
                categories.add(product.getCategoryName()));

        return categories;

    }
}
