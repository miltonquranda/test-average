package com.wawa.test.service;

import com.wawa.test.modal.Average;
import com.wawa.test.modal.Product;
import com.wawa.test.modal.ProductRepository;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class AppController extends AbstractAppController {


    public AppController(ProductRepository repository) {
        super(repository);
    }

    @PostMapping("/moving")
    public Collection<Average> movingAverage(@RequestBody List<Product> products) {

        getAllCategories(products).forEach(product -> {
            //TBD
        });

        //productCalculator.movingAverage();
        return null;

    }

    @PostMapping("/average")
    public List<Average> average(@RequestBody List<Product> products) {
        List<Average> averages = new ArrayList<>();
        //Save all the product in repository so its easy to fetch by category
        try {
            repository.saveAll(products);
            getAllCategories(products).forEach(
                    categoryName -> {
                        List<Product> productList = repository.findByCategoryName(categoryName);
                        double average = productCalculator.averageOfProduct(productList);
                        averages.add(new Average(average, categoryName));
                    }
            );
        } finally {
            repository.deleteAll(products);
        }
        return averages;
    }
}
