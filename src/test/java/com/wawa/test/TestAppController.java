package com.wawa.test;

import com.wawa.test.modal.Average;
import com.wawa.test.modal.Product;
import com.wawa.test.utils.PricingModal;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestAppController extends AbstractTest {

    @Test
    public void  testAverage() throws Exception {
        List<Product> productList = new ArrayList<>();
        productList.add(new Product(1L, "bread", PricingModal.STANDARD, 5.0d, "atrrubute1"));
        productList.add(new Product(2L, "soup", PricingModal.GOLD, 7.0d, "atrrubute2"));
        productList.add(new Product(3L, "bread", PricingModal.STANDARD, 5.0d, "atrrubute3"));
        String json = mapToJson(productList);

        String uri = "/average";
        MvcResult mvcResult = mvc.perform(
                MockMvcRequestBuilders.post(uri)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(json)
        ).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Average[] average = mapFromJson(content, Average[].class);
        assertTrue(average.length > 0);
    }
}
